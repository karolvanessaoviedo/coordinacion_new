<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentativeTensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representative_tensions', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->double('rms');
            $table->double('pico');
            $table->double('rms2');
            $table->double('pico2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representative_tensions');
    }
}
