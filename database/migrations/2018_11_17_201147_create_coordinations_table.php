<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tension_nominal');
            $table->string('tension_maxima');
            $table->string('nivel_basico_de_aislamiento');
            $table->string('frecuencia');
            $table->string('sistema_puesta_tierra');
            $table->string('altura_mar');
            $table->string('factor_correccion_altura');
            $table->string('temperatura_media');
            $table->string('nivel_de_contaminacion_ambiental');
            $table->string('distancia_de_fuga_minimo_nominal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinations');
    }
}
