<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKcd12Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kcd12', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominaln');
            $table->double('um1_1');
            $table->double('um2_1');
            $table->double('tension_residual_rayo_1');
            $table->double('ue2_pu_1');
            $table->double('ue2_kvp_1');
            $table->double('relacion_ups_1');
            $table->double('kcd_1');
            $table->double('up2_pu_1');
            $table->double('up2_kv_1');
            $table->double('relacion2_ups_1');
            $table->double('kcd2_1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kcd12');
    }
}
