<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUet8Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uet8', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal_2');
            $table->double('tension_residual_rayo');
            $table->double('fase_tierra1');
            $table->double('fase_tierra2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uet8');
    }
}
