<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUet6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uet6', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->double('um');
            $table->double('ue2');
            $table->double('up2');
            $table->null('uet');
            $table->null('upt');
            $table->double('uet2');
            $table->double('upt2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uet6');
    }
}
