<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryRepresentations5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_representations_5', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->string('sistema_puesta_tierra');
            $table->double('falla_tierra');
            $table->string('sistema');
            $table->double('valor');
            $table->double('falla_tierra1');
            $table->double('rechazo_carga1');
            $table->double('rechazo_carga2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_representations_5');
    }
}
