<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryVoltageSurgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_voltage_surges', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->double('fase_tierra1');
            $table->double('fase_tierra2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_voltage_surges');
    }
}
