<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKcd13Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kcd13', function (Blueprint $table) {
            $table->increments('id');
            $table->double('nivel_tension_kv');
            $table->double('urp_kv');
            $table->double('kcd_n');
            $table->double('ucw_kv');
            $table->double('urp_kv_1');
            $table->double('kcd_1n');
            $table->double('ucw_kv_1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kcd13');
    }
}
