<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKcd14Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kcd14', function (Blueprint $table) {
            $table->increments('id');
            $table->double('nivel_tension_kv_2');
            $table->double('urp_kv_2');
            $table->double('kcd_2n');
            $table->double('ucw_kv_2');
            $table->double('urp_kv_1_2');
            $table->double('kcd_1_2');
            $table->double('ucw_kv_1_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kcd14');
    }
}
