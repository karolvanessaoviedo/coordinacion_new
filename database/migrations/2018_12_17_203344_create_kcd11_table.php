<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKcd11Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kcd11', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('um1');
            $table->double('um2');
            $table->double('tension_residual_rayo');
            $table->double('ue2_pu');
            $table->double('ue2_kvp');
            $table->double('relacion_ups');
            $table->double('kcd');
            $table->double('up2_pu');
            $table->double('up2_kv');
            $table->double('relacion2_ups');
            $table->double('kcd2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kcd11');
    }
}
