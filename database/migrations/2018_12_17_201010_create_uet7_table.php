<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUet7Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uet7', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal_1');
            $table->double('tension_maxima_1');
            $table->double('um_1');
            $table->double('ue2_1');
            $table->double('up2_1');
            $table->double('uet_1');
            $table->double('upt_1');
            $table->double('uet2_1');
            $table->double('upt2_1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uet7');
    }
}
