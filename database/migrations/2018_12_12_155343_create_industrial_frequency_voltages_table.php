<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndustrialFrequencyVoltagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('industrial_frequency_voltages', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->double('rms');
            $table->double('pico');
            $table->double('rms2');
            $table->double('pico2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('industrial_frequency_voltages');
    }
}
