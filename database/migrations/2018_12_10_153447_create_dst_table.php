<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dst', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tension_nominal');
            $table->double('tension_maxima');
            $table->double('nivel_basico_de_aislamiento');
            $table->double('minimo');
            $table->double('seleccionado');
            $table->double('tension_asignada');
            $table->double('tension_residual_maniobra');
            $table->double('tension_residual_rayo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dst');
    }
}
