<?php

use Illuminate\Database\Seeder;
use App\Coordination;

class CoordinationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coordination = new Coordination;
        $coordination->tension_nominal = '1312321';
        $coordination->tension_maxima = '1';
        $coordination->nivel_basico_de_aislamiento = '2';
        $coordination->frecuencia = '3';
        $coordination->sistema_puesta_tierra = '4';
        $coordination->altura_mar = '5';
        $coordination->factor_correccion_altura = '6';
        $coordination->temperatura_media = '7';
        $coordination->presion_atmosferica = '8';
        $coordination->presion_atmos = 9;
        $coordination->nivel_de_contaminacion_ambiental = '10'; 
        $coordination->distancia_de_fuga_minimo_nominal = '11';
        $coordination->save();

    }
}
