<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Rutas Projecto

Route::post('coordination/create','ProjectController@store');

Route::put('coordination/update/{project}','ProjectController@update'); 

Route::get('coordination/form','ProjectController@index');

Route::get('list','ProjectController@index');

Route::delete('coordination/delete/{project}','ProjectController@destroy');

// Rutas Coordinacion

Route::post('create/coordination','CoordinationController@store');

Route::get('listform','CoordinationController@index');

//Rutas DST

Route::post('create/dst','DSTController@store');

Route::get('listdst','DSTControlleController@index');

//Rutas Tension_Representativa

Route::post('create/representative_tension','RepresentativeController@store');

Route::get('list/representative_tension','RepresentativeControlleController@index');

//Rutas Sobretensiones_representativas_temporales

Route::post('create/representative_temporary','OvervoltageController@store');

Route::get('list/representative_temporary','OvervoltageControlleController@index');


//Rutas Frecuencia Industrial

Route::post('create/frecuency_industrial','FrecuencyController@store');

Route::get('list/frecuency_industrial','FrecuencyControlleController@index');

//Rutas Tensiones de Coordinación para Sobretensiones Temporales

Route::post('create/sobretension_temporal','TemporaryOvervoltageController@store');

Route::get('list/sobretension_temporal','TemporaryOvervoltageController@index');

//Rutas Sobretensiones Representativas Energización uet_upt_5_6

Route::post('create/energization','UET6Controller@store');

Route::get('list/energization','UET6Controller@index');

//Rutas Sobretensiones Representativas Re-Energización

Route::post('create/re_energization','UET7Controller@store');

Route::get('list/re_energization','UET7Controller@index');

//Rutas Sobretensiones Representativas DST

Route::post('create/sobretension_dst','UET8Controller@store');

Route::get('list/sobretension_dst','UET8Controller@index');

//Rutas maniobras de energización

Route::post('create/sobretension_energization','KCD11Controller@store');

//Rutas maniobras de re-energización

Route::post('create/sobretension_re_energization','KCD12Controller@store');

//Rutas maniobras de energización maniuvers1

Route::post('create/maneuvers_1','KCD13Controller@store');

//Rutas maniobras de re-energización maniuvers2

Route::post('create/maneuvers2','KCD14Controller@store');





Route::group(['prefix' => 'dashboard', "middleware" => ["auth:web"]], function() {
    Route::any('{all}', function () {
        return view('home');
    })
    ->where(['all' => '.*']);
});



//Route::get('coordination','CoordinationController@index');


