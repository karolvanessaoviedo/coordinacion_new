<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kcd14 extends Model
{
    //
    protected $table = 'kcd14';

    protected $fillable = [

        'nivel_tension_kv_2',
        'urp_kv_2',
        'kcd_2n',
        'ucw_kv_2',
        'urp_kv_1_2',
        'kcd_1_2',
        'ucw_kv_1_2'


    ];
}
