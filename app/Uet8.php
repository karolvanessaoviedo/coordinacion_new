<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uet8 extends Model
{
    protected $table = 'uet8';

    protected $fillable = [

        'tension_nominal_2',
        'tension_residual_rayo',
        'fase_tierra1',
        'fase_tierra2'
    ];

}
