<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd12;

class KCD12Controller extends Controller
{
    public function index()
    {
        $kcd12 = Kcd12::all();

        return $kcd12;  
    }


    public function store(Request $request, Kcd12 $kcd12)
    {
       $kcd12 = Kcd12::create($request->all());

        return $kcd12;

    }
}
