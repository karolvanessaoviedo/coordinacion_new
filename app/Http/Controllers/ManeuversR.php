<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd_8_12;

class ManeuversR extends Controller
{
    public function index()
    {
        $maneuversr = Kcd_8_12::all();

        return $maneuversr;  
    }


    public function store(Request $request, Kcd_8_12 $maneuversr)
    {
        $maneuversr = Kcd_8_12::create($request->all());

        return $maneuversr;

    }
}
