<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd11;

class KCD11Controller extends Controller
{
    //
    public function index()
    {
        $kcd11 = Kcd11::all();

        return $kcd11;  
    }


    public function store(Request $request, Kcd11 $kcd11)
    {
       $kcd11 = Kcd11::create($request->all());

        return $kcd11;

    }
}
