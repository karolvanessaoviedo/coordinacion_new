<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uet7;


class UET7Controller extends Controller
{
     public function index()
    {
        $uet7 = Uet7::all();

        return $uet7;  
    }


    public function store(Request $request, Uet7 $uet7)
    {
       $uet7 = Uet7::create($request->all());

        return $uet7;

    }
}
