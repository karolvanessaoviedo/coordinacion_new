<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uet6;

class UET6Controller extends Controller
{
    //
    public function index()
    {
        $uet6 = Uet6::all();

        return $uet6;  
    }


    public function store(Request $request, Uet6 $uet6)
    {
       $uet6 = Uet6::create($request->all());

        return $uet6;

    }
}
