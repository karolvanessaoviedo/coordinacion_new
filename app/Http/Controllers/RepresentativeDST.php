<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Uet_Upt_8;

class RepresentativeDST extends Controller
{
    public function index()
    {
        $urp_fi = Uet_Upt_8::all();

        return $urp_fi;  
    }


    public function store(Request $request, Uet_Upt_8 $urp_fi)
    {
       $urp_fi = Uet_Upt_8::create($request->all());

        return $urp_fi;

    }
}
