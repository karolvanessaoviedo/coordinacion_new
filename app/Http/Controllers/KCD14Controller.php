<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd14;

class KCD14Controller extends Controller
{
    //
    public function index()
    {
        $kcd14 = Kcd14::all();

        return $kcd14;  
    }


    public function store(Request $request, Kcd14 $kcd14)
    {
       $kcd14 = Kcd14::create($request->all());

        return $kcd14;

    }
}
