<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Temporary_Representations_5;

class OvervoltageController extends Controller
{
    public function index()
    {
        $temporary = Temporary_Representations_5::all();

        return $temporary;  
    }


    public function store(Request $request, Temporary_Representations_5 $temporary)
    {
        $temporary = Temporary_Representations_5::create($request->all());

        return $temporary;

    }
}
