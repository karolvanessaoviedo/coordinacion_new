<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Industrial_Frequency;

class FrecuencyController extends Controller
{
    public function index()
    {
        $frecuency = Industrial_Frequency::all();

        return $frecuency;  
    }


    public function store(Request $request, Industrial_Frequency $frecuency)
    {
       $frecuency = Industrial_Frequency::create($request->all());

        return $frecuency;

    }
}
