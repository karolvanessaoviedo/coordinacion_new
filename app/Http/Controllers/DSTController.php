<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DST;

class DSTController extends Controller
{
    public function index()
    {
        $dst = DST::all();

        return $dst;  
    }


    public function store(Request $request, DST $dst)
    {
       $dst = DST::create($request->all());

        return $dst;

    }
    
}
