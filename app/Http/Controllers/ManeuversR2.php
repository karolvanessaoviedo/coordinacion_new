<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd_8_14;

class ManeuversR2 extends Controller
{
    public function index()
    {
        $maneuversr2 = Kcd_8_14::all();

        return $maneuversr2;  
    }


    public function store(Request $request, Kcd_8_14 $maneuversr2)
    {
        $maneuversr2 = Kcd_8_14::create($request->all());

        return $maneuversr2;

    }
}
