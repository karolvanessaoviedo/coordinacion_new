<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coordination;
use App\User;
use App\Project;

class ProjectController extends Controller
{
    
    public function index()
    {
        $project = Project::all();

        return $project;  
    }


    public function store(Request $request, Project $project)
    {
       $project = Project::create($request->all());

       return $project;
    
    }

    public function update(Request $request, Project $project)
    {
       $project->fill($request->all());

       $project->save();

       return $project;
    }

    public function destroy(Project $project)  
    {

        $project->delete();

        return $project;

    }
}
