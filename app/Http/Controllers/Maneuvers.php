<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Kcd_8_11;

class Maneuvers extends Controller
{
    public function index()
    {
        $maneuvers = Kcd_8_11::all();

        return $maneuvers;  
    }


    public function store(Request $request, Kcd_8_11 $maneuvers)
    {
        $maneuvers = Kcd_8_11::create($request->all());

        return $maneuvers;

    }
}
