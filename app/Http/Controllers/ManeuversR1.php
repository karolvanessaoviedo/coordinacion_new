<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd_8_13;

class ManeuversR1 extends Controller
{
    public function index()
    {
        $maneuversr1 = Kcd_8_13::all();

        return $maneuversr1;  
    }


    public function store(Request $request, Kcd_8_13 $maneuversr1)
    {
        $maneuversr1 = Kcd_8_13::create($request->all());

        return $maneuversr1;

    }
}
