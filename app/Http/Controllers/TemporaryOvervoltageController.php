<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Temporary_Voltage_Surges;

class TemporaryOvervoltageController extends Controller
{
    public function index()
    {
        $tempory = Temporary_Voltage_Surges::all();

        return $tempory;  
    }


    public function store(Request $request,Temporary_Voltage_Surges $tempory)
    {
       $tempory = Temporary_Voltage_Surges::create($request->all());

        return $tempory;

    }
}
