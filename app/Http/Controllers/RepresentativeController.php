<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RepresentativeTensions;

class RepresentativeController extends Controller
{
    public function index()
    {
        $representative = RepresentativeTensions::all();

        return $representative;  
    }


    public function store(Request $request, RepresentativeTensions $representative)
    {
       $representative = RepresentativeTensions::create($request->all());

        return $representative;

    }
}
