<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uet8;

class UET8Controller extends Controller
{
    public function index()
    {
        $uet8 = Uet8::all();

        return $uet8;  
    }


    public function store(Request $request, Uet8 $uet8)
    {
       $uet8 = Uet8::create($request->all());

        return $uet8;

    }
}
