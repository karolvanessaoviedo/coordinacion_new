<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kcd13;

class KCD13Controller extends Controller
{
    //
    public function index()
    {
        $kcd13 = Kcd13::all();

        return $kcd13;  
    }


    public function store(Request $request, Kcd13 $kcd13)
    {
       $kcd13 = Kcd13::create($request->all());

        return $kcd13;

    }
}
