<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coordination;

class CoordinationController extends Controller
{
    public function index()
    {
        $user = Coordination::all();

        return $user;  
    }


    public function store(Request $request, Coordination $coordination)
    {
       $coordination = Coordination::create($request->all());

        return $coordination;

    }


    public function update(Request $request, Coordination $coordination)
    {
        $coordination->fill($request->all());

        $coordination->save();
 
        return $coordination;
        
    }

    public function destroy(Coordination $coordination)
    {
        
    }
}
