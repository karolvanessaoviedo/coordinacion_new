<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordination extends Model
{
    //
    protected $table = 'coordinations';

    protected $fillable = [
        
    'tension_nominal',
    'tension_maxima',
    'nivel_basico_de_aislamiento',
    'frecuencia', 
    'sistema_puesta_tierra',
    'altura_mar',
    'factor_correccion_altura',
    'temperatura_media',
    'presion_atmosferica',
    'presion_atmos',
    'nivel_de_contaminacion_ambiental',
    'distancia_de_fuga_minimo_nominal'
    


    ];

}
