<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uet6 extends Model
{
    protected $table = 'uet6';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'um',
        'ue2',
        'up2',
        'uet1',
        'upt1',
        'uet2',
        'upt2'


    ];
}
