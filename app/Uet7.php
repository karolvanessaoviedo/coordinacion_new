<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uet7 extends Model
{
    protected $table = 'uet7';

    protected $fillable = [

        'tension_nominal_1',
        'tension_maxima_1',
        'um_1',
        'ue2_1',
        'up2_1',
        'uet_1',
        'upt_1',
        'uet2_1',
        'upt2_1'
        
    ];

}
