<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporary_Voltage_Surges extends Model
{
    protected $table = 'temporary_voltage_surges';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'fase_tierra1',
        'fase_tierra2'

    ];
}


