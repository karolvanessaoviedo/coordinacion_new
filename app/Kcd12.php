<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kcd12 extends Model
{
    protected $table = 'kcd12';

    protected $fillable = [

        'tension_nominaln',
        'um1_1',
        'um2_1',
        'tension_residual_rayo_1',
        'ue2_pu_1',
        'ue2_kvp_1',
        'relacion_ups_1',
        'kcd_1',
        'up2_pu_1',
        'up2_kv_1',
        'relacion2_ups_1',
        'kcd2_1'


    ];
}
