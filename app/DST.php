<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DST extends Model
{
    protected $table = 'dst';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'nivel_basico_de_aislamiento',
        'minimo',
        'seleccionado',
        'tension_asignada',
        'tension_residual_maniobra',
        'tension_residual_rayo'
        
    ];
}
