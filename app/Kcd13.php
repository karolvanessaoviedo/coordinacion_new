<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kcd13 extends Model
{
    //
    protected $table = 'kcd13';

    protected $fillable = [

        'nivel_tension_kv',
        'urp_kv',
        'kcd_n',
        'ucw_kv',
        'urp_kv_1',
        'kcd_1n',
        'ucw_kv_1'

    ];
}
