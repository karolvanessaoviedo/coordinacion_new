<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kcd11 extends Model
{
    protected $table = 'kcd11';

    protected $fillable = [

        'tension_nominal',
        'um1',
        'um2',
        'tension_residual_rayo',
        'ue2_pu',
        'ue2_kvp',
        'relacion_ups',
        'kcd',
        'up2_pu',
        'up2_kv',
        'relacion2_ups',
        'kcd2'


    ];
}
