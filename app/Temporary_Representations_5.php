<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporary_Representations_5 extends Model
{
    //
    protected $table = 'temporary_representations_5';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'sistema_puesta_tierra',
        'falla_tierra',
        'sistema',
        'valor',
        'falla_tierra1',
        'rechazo_carga1',
        'rechazo_carga2'



    ];
}
