<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industrial_Frequency extends Model
{
    protected $table = 'industrial_frequency_voltages';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'rms',
        'pico',
        'rms2',
        'pico2'


    ];
}
