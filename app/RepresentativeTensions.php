<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepresentativeTensions extends Model
{
    protected $table = 'representative_tensions';

    protected $fillable = [

        'tension_nominal',
        'tension_maxima',
        'rms',
        'pico',
        'rms2',
        'pico2'

    ];
}
