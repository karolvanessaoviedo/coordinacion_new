import Project from './components/Project'
import Inicio from './components/Inicio'
import Form from './components/Form'
import List from './components/List'
import Coordination from './components/Coordination'
import ListForm from './components/ListForm'
import Tabla2 from './components/Tabla2'
import Tabla3 from './components/Tabla3'
import Tabla4 from './components/Tabla4'
import Tabla5 from './components/Tabla5'
import Tabla6 from './components/Tabla6'
import Tabla7 from './components/Tabla7'
import Tabla8 from './components/Tabla8'


const routes = [
    {

        path: '/dashboard/project',
        name: 'project',
        component: Project

    },

    {

        path: '/dashboard/inicio',
        name: 'inicio',
        component: Inicio,
        props: true

    },

    {

        path: '/dashboard/form/:project_id',
        name: 'form',
        component: Form,
        props: true

    },

    {

        path: '/dashboard/list/: id',
        name: 'list',
        component: List,
        props: true

    },

    {

        path: '/dashboard/coordination',
        name : 'coordination',
        component: Coordination

    },
    
    {
        path: '/dashboard/listform',
        name : 'listform',
        component: ListForm
    },

    {
        path: '/dashboard/tabla2',
        name : 'tabla2',
        component: Tabla2
    },

    {
        path: '/dashboard/tabla3',
        name : 'tabla3',
        component: Tabla3
    },

    {
        path: '/dashboard/tabla4',
        name: 'tabla4',
        component: Tabla4
    },

    {
        path: '/dashboard/tabla5',
        name: 'tabla5',
        component: Tabla5
    },

    {
        path: '/dashboard/tabla6',
        name: 'tabla6',
        component: Tabla6
    },

    {
        path: '/dashboard/tabla7',
        name: 'tabla7',
        component: Tabla7
    },

    {
        path: '/dashboard/tabla8',
        name: 'tabla8',
        component: Tabla8
    }


];

export default routes