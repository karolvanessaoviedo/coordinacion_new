
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from "vue-router"
import routes from './routes'

Vue.use(VueRouter)
const router = new VueRouter({
    routes,
    mode: 'history'
})





/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('coordination', require('./components/Coordination.vue'));
Vue.component('project', require('./components/Project.vue'));
Vue.component('inicio', require('./components/Inicio.vue'));
Vue.component('layout', require('./components/Layout.vue'));
Vue.component('form', require('./components/Form.vue'));
Vue.component('list', require('./components/List.vue'));
Vue.component('listform', require('./components/ListForm.vue'));
Vue.component('tabla2', require('./components/Tabla2.vue'));
Vue.component('tabla3', require('./components/Tabla3.vue'));
Vue.component('tabla4', require('./components/Tabla4.vue'));
Vue.component('tabla5', require('./components/Tabla5.vue'));
Vue.component('tabla6', require('./components/Tabla6.vue'));
Vue.component('tabla7', require('./components/Tabla7.vue'));
Vue.component('tabla8', require('./components/Tabla8.vue'));





 




 // Se usa para obtener la url del proyecto de forma global en todos los componentes
Vue.mixin({
    data() {
        return {
            app_url: process.env.MIX_APP_URL
        }
    },
})

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app',
   router
});